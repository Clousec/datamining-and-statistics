# Hogeschool Inholland Datamining and Statistics

## Table of contents

* [Introduction](#introduction)
* [Assessment 1](#assessment-1)
* [Assessment 2](#assessment-2)

## Introduction
This repository contains assessments for the Datamining and Statistics Course on Hogeschool Inholland.
Read the matching folder for more information per assessment.

## Assessment 1
! There is more information inside the `Assessment 1` folder !

## Assessment 2
! There is more information inside the `Assessment 2` folder !

Assessment consists of dataset and `.ipynb` file that contains linear regression and multi linear regression.
Dataset contains NASA Near Earth Objects data.